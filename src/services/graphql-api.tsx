import { ApolloClient, InMemoryCache, ApolloProvider, gql } from '@apollo/client';
import {GameType} from "../components/Types";

const data = [
    {
        'id': "abec-123",
        'date': '2022-07-04 11:45:00 UTC',
        'category': {
            'id': '321',
            'name': 'soccer'
        },
        'homeTeam': {
            'id': '123-abc',
            'name': "team1",
            'score': 0
        },
        'awayTeam': {
            'id': '123-abc2',
            'name': "team2",
            'score': 0
        },
        'odds': {
            'home': 1.48,
            'draw': 3.45,
            'away': 4.40,
        },
    },
    {
        'id': "abec-123",
        'date': '2022-07-04 11:45:00 UTC',
        'category': {
            'id': '322',
            'name': 'baseball'
        },
        'homeTeam': {
            'id': '123-abc',
            'name': "team1",
            'score': 0
        },
        'awayTeam': {
            'id': '123-abc2',
            'name': "team2",
            'score': 0
        },
        'odds': {
            'home': null,
            'draw': 3.45,
            'away': null,
        },
    },
    {
        'id': "abec-123",
        'category': {
            'id': '321',
            'name': 'soccer'
        },
        'date': '2022-07-04 11:45:00 UTC',
        'homeTeam': {
            'id': '123-abc',
            'name': "team1",
            'score': 0
        },
        'awayTeam': {
            'id': '123-abc2',
            'name': "team2",
            'score': 0
        },
        'odds': {
            'home': 1.48,
            'draw': 3.45,
            'away': 4.40,
        },
    },
];

export const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql',
    cache: new InMemoryCache(),
});

export const QUERY = gql`
    query {
        games {
            id
            date
            category {
                id
                name
            }
            home {
                id
                name
                score
            }
            away {
                id
                name
                score
            }
            odds {
                home
                draw
                away
            }
        }
    }
`;

export const SUBSCRIPTION = gql`
    subscription {
        gameUpdate {
            id
            date
            category {
                id
                name
            }
            home {
                id
                name
                score
            }
            away {
                id
                name
                score
            }
            odds {
                home
                draw
                away
            }
        }
    }
`;

export const getGamesData = (): GameType[] => {
    // client.subscribe({
    //         query: QUERY_DATA
    //     })
    //     .then((result) => {
    //         console.log(result);
    //     });

    return data;
}

