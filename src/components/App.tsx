import React, { useState, useEffect } from 'react';
import './App.css';
import Game from "./Game";
import {GameType} from "./Types";
import { Container } from '@mui/material';
import {getGamesData, QUERY, client, SUBSCRIPTION} from "../services/graphql-api";
import {useQuery, useSubscription} from '@apollo/client';

enum STATUSES {
    idle,
    loading,
    error,
    success
}

function App() {
    const [gamesData, setGamesData] = useState<GameType[] | null>(null);
    const [appStatus, setAppStatus] = useState<STATUSES>(STATUSES.idle);
    const { data, loading, subscribeToMore } = useQuery(QUERY, {client: client});
    console.log('data', data);
    console.log('loading', loading);

    useEffect(() => {
        setGamesData(getGamesData());
        setAppStatus(STATUSES.success);
        subscribeToMore({
            document: SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                console.log('prev', prev);
                console.log('subscriptionData', subscriptionData);
                if (!subscriptionData.data) return prev;

                const newFeedItem = subscriptionData.data.gameUpdated;

                return Object.assign({}, prev, {
                    data: {
                        games: [newFeedItem, prev]
                    }
                });
            }
        });
    });

  return (
      <div className="App">
          <Container>
              {appStatus === STATUSES.idle && <> :-) </>}
              {loading && <> Loading data... </>}
              {!loading && gamesData && gamesData?.length > 0 && gamesData.map((game: GameType) => <Game data={game} />)}
              {appStatus === STATUSES.error && <> Error </>}
          </Container>
      </div>
  );
}

export default App;
