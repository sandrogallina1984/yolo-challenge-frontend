import React from 'react';
import type {GameType} from "./Types";
import Grid from '@mui/material/Grid';
import Team from "./Team";
import Odds from "./Odds";
import {Box} from "@mui/material";

function Game({data}: { data:GameType}) {
    const {date, homeTeam, awayTeam, odds, category} = data;
    const dateTime = new Date(date);
    return (
        <Grid container spacing={2} className="game">
            <Grid item xs={6}>
                <Team team={homeTeam}/>
                <Team team={awayTeam}/>
                <Box className="date">
                    <p>Category: {category.name}</p>
                    <p>Started: {dateTime.toLocaleString()}</p>
                    <p>Updated: {dateTime.toLocaleString()}</p>
                </Box>
            </Grid>
            <Grid item xs={6}>
                <Odds odds={odds}/>
            </Grid>
        </Grid>
    )
}

export default Game;