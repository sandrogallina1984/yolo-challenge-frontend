import React from 'react';
import type {TeamType} from "./Types";
import Grid from '@mui/material/Grid';

function Team ({team}:{team:TeamType}) {
    return (
        <Grid container spacing={2}>
            <Grid item xs={9}>
                {team.name}
            </Grid>
            <Grid item xs={3} className="score">
                {team.score}
            </Grid>
        </Grid>
    );
}

export default Team;