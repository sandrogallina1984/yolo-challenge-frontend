import React from 'react';

export type TeamType = {
    id: string,
    name: string,
    score: number,
}

export type OddsType = {
    home: number | null,
    draw: number | null,
    away: number | null,
}

export type Category = {
    id: string,
    name: string
}

export type GameType = {
    id: string,
    date: string,
    category: Category,
    homeTeam: TeamType,
    awayTeam: TeamType,
    odds: OddsType
}
