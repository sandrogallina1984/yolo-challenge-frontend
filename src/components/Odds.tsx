import React from 'react';
import type {OddsType} from "./Types";
import Grid from '@mui/material/Grid';

function Odds ({odds}:{odds:OddsType}) {
    return (
        <Grid container columnSpacing={10}>
            <Grid item xs={4} marginTop="3vh">
                {odds?.home || '-' }
            </Grid>
            <Grid item xs={4} marginTop="3vh">
                {odds?.draw || '-'}
            </Grid>
            <Grid item xs={4} marginTop="3vh">
                {odds?.away || '-'}
            </Grid>
        </Grid>
    );
}

export default Odds;